# diesel-mysql-spatial - An extension for the Diesel framework to support MySQL spatial datatypes

[![Crates.io](https://img.shields.io/crates/v/diesel-mysql-spatial.svg)](https://crates.io/crates/diesel-mysql-spatial)
[![Documentation](https://docs.rs/diesel-mysql-spatial/badge.svg)](https://docs.rs/diesel-mysql-spatial/)
[![License](https://img.shields.io/crates/l/diesel-mysql-spatial.svg)](https://www.mozilla.org/en-US/MPL/2.0/)

## Usage

1. Add the following to your Cargo.toml:
```toml
[dependencies]
diesel-mysql-spatial = "0.1"
```

2. Read the [crate documentation](https://docs.rs/diesel-mysql-spatial/)

## Example

Assuming a MySQL table defined like this

```mysql
CREATE TABLE districts (
    id         INTEGER NOT NULL PRIMARY KEY,
    center     POINT NOT NULL,
    area       POLYGON NOT NULL
);
```

then the Rust code may look like this:

```rust
use diesel_mysql_spatial::data_types::{Point, Polygon};

#[derive(Insertable, Queryable)]
#[table_name = "districts"]
struct District {
    id: i32,
    center: Point,
    area: Polygon,
}

table! {
    use diesel_mysql_spatial::sql_types::*;
    use diesel::sql_types::*;
    districts (id) {
        id -> Integer,
        center -> Point,
        area -> Polygon,
    }
}
```

## Stability

This crate follows [semantic versioning](http://semver.org) with the additional
promise that below `1.0.0` backwards-incompatible changes will not be
introduced with only a patch-level version number change.

## License

Licensed under Mozilla Public License, Version 2.0 ([LICENSE](LICENSE)
or https://www.mozilla.org/en-US/MPL/2.0/).

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, shall be licensed as above
including compatibility with secondary licenses, as defined by the MPL.

<!--
© 2022 Christoph Grenz <https://grenz-bonn.de>
SPDX-License-Identifier: MPL-2.0
-->
