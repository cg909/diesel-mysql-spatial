// © 2022 Christoph Grenz <https://grenz-bonn.de>
//
// SPDX-License-Identifier: MPL-2.0

/// The MySQL `GEOMETRY` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct Geometry;

/// The MySQL `POINT` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct Point;

/// The MySQL `POLYGON` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct Polygon;

/// The MySQL `LINESTRING` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct LineString;

/// The MySQL `MULTIPOINT` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct MultiPoint;

/// The MySQL `MULTILINESTRING` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct MultiLineString;

/// The MySQL `MULTIPOLYGON` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct MultiPolygon;

/// The MySQL `GEOMETRYCOLLECTION` type.
#[derive(Debug, Clone, Copy, Default, QueryId, SqlType)]
#[mysql_type = "Blob"]
pub struct GeometryCollection;

impl From<Point> for Geometry {
	fn from(_: Point) -> Geometry {
		Geometry
	}
}
impl From<Polygon> for Geometry {
	fn from(_: Polygon) -> Geometry {
		Geometry
	}
}
impl From<LineString> for Geometry {
	fn from(_: LineString) -> Geometry {
		Geometry
	}
}
impl From<MultiPoint> for Geometry {
	fn from(_: MultiPoint) -> Geometry {
		Geometry
	}
}
impl From<MultiLineString> for Geometry {
	fn from(_: MultiLineString) -> Geometry {
		Geometry
	}
}
impl From<MultiPolygon> for Geometry {
	fn from(_: MultiPolygon) -> Geometry {
		Geometry
	}
}
impl From<GeometryCollection> for Geometry {
	fn from(_: GeometryCollection) -> Geometry {
		Geometry
	}
}
