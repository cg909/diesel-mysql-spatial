// © 2022 Christoph Grenz <https://grenz-bonn.de>
//
// SPDX-License-Identifier: MPL-2.0

#![allow(non_camel_case_types)]
use super::functions;

/// The return type of [`ST_Dimension(…)`][`functions::ST_Dimension()`]
pub type ST_Dimension<G, Expr1> = functions::ST_Dimension::HelperType<G, Expr1>;

/// The return type of [`ST_Envelope(…)`][`functions::ST_Envelope()`]
pub type ST_Envelope<G, Expr1> = functions::ST_Envelope::HelperType<G, Expr1>;

/// The return type of [`ST_GeometryType(…)`][`functions::ST_GeometryType()`]
pub type ST_GeometryType<G, Expr1> = functions::ST_GeometryType::HelperType<G, Expr1>;

/// The return type of [`ST_IsEmpty(…)`][`functions::ST_IsEmpty()`]
pub type ST_IsEmpty<G, Expr1> = functions::ST_IsEmpty::HelperType<G, Expr1>;

/// The return type of [`ST_IsSimple(…)`][`functions::ST_IsSimple()`]
pub type ST_IsSimple<G, Expr1> = functions::ST_IsSimple::HelperType<G, Expr1>;

/// The return type of [`ST_SRID(…)`][`functions::ST_SRID()]
pub type ST_SRID<G, Expr1> = functions::ST_SRID::HelperType<G, Expr1>;

/// The return type of [`ST_SRID_2(…)`][`functions::ST_SRID_2()`]
pub type ST_SRID_2<G, Expr1, Expr2> = functions::ST_SRID_2::HelperType<G, Expr1, Expr2>;

/// The return type of [`ST_Transform(…)`][`functions::ST_Transform()`]
pub type ST_Transform<G, Expr1, Expr2> = functions::ST_Transform::HelperType<G, Expr1, Expr2>;

/// The return type of [`ST_Latitude(…)`][`functions::ST_Latitude()`]
pub type ST_Latitude<Expr> = functions::ST_Latitude::HelperType<Expr>;

/// The return type of [`ST_Longitude(…)`][`functions::ST_Longitude()`]
pub type ST_Longitude<Expr> = functions::ST_Longitude::HelperType<Expr>;

/// The return type of [`ST_Latitude_2(…)`][`functions::ST_Latitude_2()`]
pub type ST_Latitude_2<Expr1, Expr2> = functions::ST_Latitude_2::HelperType<Expr1, Expr2>;

/// The return type of [`ST_Longitude_2(…)`][`functions::ST_Longitude_2()`]
pub type ST_Longitude_2<Expr1, Expr2> = functions::ST_Longitude_2::HelperType<Expr1, Expr2>;

/// The return type of [`ST_X(…)`][`functions::ST_X()`]
pub type ST_X<Expr> = functions::ST_X::HelperType<Expr>;

/// The return type of [`ST_Y(…)`][`functions::ST_Y()`]
pub type ST_Y<Expr> = functions::ST_Y::HelperType<Expr>;

/// The return type of [`ST_X_2(…)`][`functions::ST_X_2()`]
pub type ST_X_2<Expr1, Expr2> = functions::ST_X_2::HelperType<Expr1, Expr2>;

/// The return type of [`ST_Y_2(…)`][`functions::ST_Y_2()`]
pub type ST_Y_2<Expr1, Expr2> = functions::ST_Y_2::HelperType<Expr1, Expr2>;

/// The return type of [`ST_EndPoint(…)`][`functions::ST_EndPoint()`]
pub type ST_EndPoint<Expr> = functions::ST_EndPoint::HelperType<Expr>;

/// The return type of [`ST_IsClosed(…)`][`functions::ST_IsClosed()`]
pub type ST_IsClosed<G, Expr1> = functions::ST_IsClosed::HelperType<G, Expr1>;

/// The return type of [`ST_Length(…)`][`functions::ST_Length()`]
pub type ST_Length<G, Expr1> = functions::ST_Length::HelperType<G, Expr1>;

/// The return type of [`ST_NumPoints(…)`][`functions::ST_NumPoints()`]
pub type ST_NumPoints<Expr> = functions::ST_NumPoints::HelperType<Expr>;

/// The return type of [`ST_PointN(…)`][`functions::ST_PointN()`]
pub type ST_PointN<Expr1, Expr2> = functions::ST_PointN::HelperType<Expr1, Expr2>;

/// The return type of [`ST_StartPoint(…)`][`functions::ST_StartPoint()`]
pub type ST_StartPoint<Expr> = functions::ST_StartPoint::HelperType<Expr>;

/// The return type of [`ST_Area(…)`][`functions::ST_Area()`]
pub type ST_Area<G, Expr1> = functions::ST_Area::HelperType<G, Expr1>;

/// The return type of [`ST_Centroid(…)`][`functions::ST_Centroid()`]
pub type ST_Centroid<G, Expr1> = functions::ST_Centroid::HelperType<G, Expr1>;

/// The return type of [`ST_ExteriorRing(…)`][`functions::ST_ExteriorRing()`]
pub type ST_ExteriorRing<Expr> = functions::ST_ExteriorRing::HelperType<Expr>;

/// The return type of [`ST_InteriorRingN(…)`][`functions::ST_InteriorRingN()`]
pub type ST_InteriorRingN<Expr1, Expr2> = functions::ST_InteriorRingN::HelperType<Expr1, Expr2>;

/// The return type of [`ST_NumInteriorRings(…)`][`functions::ST_NumInteriorRings()`]
pub type ST_NumInteriorRings<Expr> = functions::ST_NumInteriorRings::HelperType<Expr>;

/// The return type of [`ST_GeometryN(…)`][`functions::ST_GeometryN()`]
pub type ST_GeometryN<Expr1, Expr2> = functions::ST_GeometryN::HelperType<Expr1, Expr2>;

/// The return type of [`ST_NumGeometries(…)`][`functions::ST_NumGeometries()`]
pub type ST_NumGeometries<Expr> = functions::ST_NumGeometries::HelperType<Expr>;

/// The return type of [`ST_Buffer_Strategy(…)`][`functions::ST_Buffer_Strategy()`]
pub type ST_Buffer_Strategy<Expr> = functions::ST_Buffer_Strategy::HelperType<Expr>;

/// The return type of [`ST_Buffer_Strategy_2(…)`][`functions::ST_Buffer_Strategy_2()`]
pub type ST_Buffer_Strategy_2<Expr1, Expr2> =
	functions::ST_Buffer_Strategy_2::HelperType<Expr1, Expr2>;

/// The return type of [`ST_Buffer(…)`][`functions::ST_Buffer()`]
pub type ST_Buffer<G, Expr1, Expr2> = functions::ST_Buffer::HelperType<G, Expr1, Expr2>;

/// The return type of [`ST_Buffer_1(…)`][`functions::ST_Buffer_1()`]
pub type ST_Buffer_1<G, Expr1, Expr2, Expr3> =
	functions::ST_Buffer_1::HelperType<G, Expr1, Expr2, Expr3>;

/// The return type of [`ST_Buffer_2(…)`][`functions::ST_Buffer_2()`]
pub type ST_Buffer_2<G, Expr1, Expr2, Expr3, Expr4> =
	functions::ST_Buffer_2::HelperType<G, Expr1, Expr2, Expr3, Expr4>;

/// The return type of [`ST_Buffer_3(…)`][`functions::ST_Buffer_3()`]
pub type ST_Buffer_3<G, Expr1, Expr2, Expr3, Expr4, Expr5> =
	functions::ST_Buffer_3::HelperType<G, Expr1, Expr2, Expr3, Expr4, Expr5>;

/// The return type of [`ST_ConvexHull(…)`][`functions::ST_ConvexHull()`]
pub type ST_ConvexHull<G, Expr1> = functions::ST_ConvexHull::HelperType<G, Expr1>;

/// The return type of [`ST_Difference(…)`][`functions::ST_Difference()`]
pub type ST_Difference<G1, G2, Expr1, Expr2> =
	functions::ST_Difference::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Intersection(…)`][`functions::ST_Intersection()`]
pub type ST_Intersection<G1, G2, Expr1, Expr2> =
	functions::ST_Intersection::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_SymDifference(…)`][`functions::ST_SymDifference()`]
pub type ST_SymDifference<G1, G2, Expr1, Expr2> =
	functions::ST_SymDifference::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Union(…)`][`functions::ST_Union()`]
pub type ST_Union<G1, G2, Expr1, Expr2> = functions::ST_Union::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_LineInterpolatePoint(…)`][`functions::ST_LineInterpolatePoint()`]
pub type ST_LineInterpolatePoint<Expr1, Expr2> =
	functions::ST_LineInterpolatePoint::HelperType<Expr1, Expr2>;

/// The return type of [`ST_LineInterpolatePoints(…)`][`functions::ST_LineInterpolatePoints()`]
pub type ST_LineInterpolatePoints<Expr1, Expr2> =
	functions::ST_LineInterpolatePoints::HelperType<Expr1, Expr2>;

/// The return type of [`ST_PointAtDistance(…)`][`functions::ST_PointAtDistance()`]
pub type ST_PointAtDistance<Expr1, Expr2> = functions::ST_PointAtDistance::HelperType<Expr1, Expr2>;

/// The return type of [`ST_Contains(…)`][`functions::ST_Contains()`]
pub type ST_Contains<G1, G2, Expr1, Expr2> =
	functions::ST_Contains::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Crosses(…)`][`functions::ST_Crosses()`]
pub type ST_Crosses<G1, G2, Expr1, Expr2> = functions::ST_Crosses::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Disjoint(…)`][`functions::ST_Disjoint()`]
pub type ST_Disjoint<G1, G2, Expr1, Expr2> =
	functions::ST_Disjoint::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Distance(…)`][`functions::ST_Distance()`]
pub type ST_Distance<G1, G2, Expr1, Expr2> =
	functions::ST_Distance::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Equals(…)`][`functions::ST_Equals()`]
pub type ST_Equals<G1, G2, Expr1, Expr2> = functions::ST_Equals::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Intersects(…)`][`functions::ST_Intersects()`]
pub type ST_Intersects<G1, G2, Expr1, Expr2> =
	functions::ST_Intersects::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Overlaps(…)`][`functions::ST_Overlaps()`]
pub type ST_Overlaps<G1, G2, Expr1, Expr2> =
	functions::ST_Overlaps::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Touches(…)`][`functions::ST_Touches()`]
pub type ST_Touches<G1, G2, Expr1, Expr2> = functions::ST_Touches::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Within(…)`][`functions::ST_Within()`]
pub type ST_Within<G1, G2, Expr1, Expr2> = functions::ST_Within::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBRContains(…)`][`functions::MBRContains()`]
pub type MBRContains<G1, G2, Expr1, Expr2> =
	functions::MBRContains::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBRCoveredBy(…)`][`functions::MBRCoveredBy()`]
pub type MBRCoveredBy<G1, G2, Expr1, Expr2> =
	functions::MBRCoveredBy::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBRCovers(…)`][`functions::MBRCovers()`]
pub type MBRCovers<G1, G2, Expr1, Expr2> = functions::MBRCovers::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBRDisjoint(…)`][`functions::MBRDisjoint()`]
pub type MBRDisjoint<G1, G2, Expr1, Expr2> =
	functions::MBRDisjoint::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBREquals(…)`][`functions::MBREquals()`]
pub type MBREquals<G1, G2, Expr1, Expr2> = functions::MBREquals::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBROverlaps(…)`][`functions::MBROverlaps()`]
pub type MBROverlaps<G1, G2, Expr1, Expr2> =
	functions::MBROverlaps::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBRTouches(…)`][`functions::MBRTouches()`]
pub type MBRTouches<G1, G2, Expr1, Expr2> = functions::MBRTouches::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`MBRWithin(…)`][`functions::MBRWithin()`]
pub type MBRWithin<G1, G2, Expr1, Expr2> = functions::MBRWithin::HelperType<G1, G2, Expr1, Expr2>;

/// The return type of [`ST_Collect(…)`][`functions::ST_Collect()`]
pub type ST_Collect<G, Expr1> = functions::ST_Collect::HelperType<G, Expr1>;

/// The return type of [`ST_IsValid(…)`][`functions::ST_IsValid()`]
pub type ST_IsValid<G, Expr1> = functions::ST_IsValid::HelperType<G, Expr1>;

/// The return type of [`ST_Validate(…)`][`functions::ST_Validate()`]
pub type ST_Validate<G, Expr> = functions::ST_Validate::HelperType<G, Expr>;
