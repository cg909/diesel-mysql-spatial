// © 2022 Christoph Grenz <https://grenz-bonn.de>
//
// SPDX-License-Identifier: MPL-2.0

use super::sql_types;
use crate::GeometryError;
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use diesel::deserialize;
use diesel::deserialize::FromSql;
use diesel::mysql::Mysql;
use diesel::serialize;
use diesel::serialize::{IsNull, Output, ToSql};
use geo_types;
use std::io::{Cursor, Write};
use std::ops::{Deref, DerefMut};
use wkb::{WKBReadExt, WKBWriteExt};

/// Spatial Reference Identifier
///
/// `0` is the unitless Cartesian plane, other values are EPSG SRS IDs like `4326` for WGS 84 or
/// identify custom reference systems registered with `CREATE SPATIAL REFERENCE SYSTEM`.
pub type SRID = u32;

/// SRID for the unitless Cartesian plane
pub const SRID_CARTESIAN: SRID = 0;

/// Defines a Diesel-compatible wrapper struct for a geo type
///
/// Also implements the most important conversion traits.
macro_rules! mysql_type {
	($type:ident, $geom:ident, $sql_name:ty, $doc:literal) => {
		#[derive(Debug, Clone, PartialEq, FromSqlRow, AsExpression)]
		#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
		#[sql_type = stringify!($sql_name)]
		#[doc=$doc]
		///
		/// MySQL extension: The spatial reference system identifier (SRID) may identify the used
		/// coordinate system.
		pub struct $type {
			pub srid: SRID,
			#[cfg_attr(feature = "serde", serde(flatten))]
			pub geom: geo_types::$geom<f64>,
		}

		impl From<geo_types::$geom<f64>> for $type {
			#[inline]
			fn from(geom: geo_types::$geom<f64>) -> Self {
				Self {
					srid: 0,
					geom: geom.into(),
				}
			}
		}

		impl AsRef<geo_types::$geom<f64>> for $type {
			#[inline]
			fn as_ref(&self) -> &geo_types::$geom<f64> {
				&self.geom
			}
		}

		impl AsMut<geo_types::$geom<f64>> for $type {
			#[inline]
			fn as_mut(&mut self) -> &mut geo_types::$geom<f64> {
				&mut self.geom
			}
		}

		impl Deref for $type {
			type Target = geo_types::$geom<f64>;

			#[inline]
			fn deref(&self) -> &Self::Target {
				&self.geom
			}
		}

		impl DerefMut for $type {
			#[inline]
			fn deref_mut(&mut self) -> &mut Self::Target {
				&mut self.geom
			}
		}
	};
}

/// Implements common SQL conversion functions for a geo type and its wrapper.
macro_rules! impl_traits {
	($type:ident, $geom:ident, $sql_name:ty) => {
		impl FromSql<$sql_name, Mysql> for $type {
			fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
				let mut bytes = not_none!(bytes);
				let srid = bytes.read_u32::<LittleEndian>()?;
				let mut bytes_cursor = Cursor::new(bytes);
				let geom = bytes_cursor
					.read_wkb()
					.map_err(|e| GeometryError::from(e))?;
				if let geo_types::Geometry::$geom(p) = geom {
					Ok(Self { srid, geom: p })
				} else {
					Err(Box::new(GeometryError::WrongType))
				}
			}
		}

		impl FromSql<$sql_name, Mysql> for Geometry {
			fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
				let mut bytes = not_none!(bytes);
				let srid = bytes.read_u32::<LittleEndian>()?;
				let mut bytes_cursor = Cursor::new(bytes);
				let geom = bytes_cursor.read_wkb().map_err(GeometryError::from)?;
				Ok(Self { srid, geom })
			}
		}

		impl FromSql<$sql_name, Mysql> for geo_types::$geom<f64> {
			fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
				let wrapped = $type::from_sql(bytes)?;
				Ok(wrapped.geom)
			}
		}

		impl FromSql<$sql_name, Mysql> for geo_types::Geometry<f64> {
			fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
				let wrapped = <Geometry as FromSql<$sql_name, Mysql>>::from_sql(bytes)?;
				Ok(wrapped.geom)
			}
		}

		impl ToSql<$sql_name, Mysql> for $type {
			fn to_sql<W: Write>(&self, out: &mut Output<W, Mysql>) -> serialize::Result {
				out.write_u32::<LittleEndian>(self.srid)?;
				out.write_wkb(&geo_types::Geometry::$geom(self.geom.clone()))
					.map_err(GeometryError::from)?;
				Ok(IsNull::No)
			}
		}

		impl ToSql<sql_types::Geometry, Mysql> for $type {
			fn to_sql<W: Write>(&self, out: &mut Output<W, Mysql>) -> serialize::Result {
				out.write_u32::<LittleEndian>(self.srid)?;
				out.write_wkb(&geo_types::Geometry::$geom(self.geom.clone()))
					.map_err(GeometryError::from)?;
				Ok(IsNull::No)
			}
		}

		impl ToSql<$sql_name, Mysql> for geo_types::$geom<f64> {
			fn to_sql<W: Write>(&self, out: &mut Output<W, Mysql>) -> serialize::Result {
				out.write_u32::<LittleEndian>(0)?;
				out.write_wkb(&geo_types::Geometry::$geom(self.clone()))
					.map_err(GeometryError::from)?;
				Ok(IsNull::No)
			}
		}

		impl ToSql<$sql_name, Mysql> for geo_types::Geometry<f64> {
			fn to_sql<W: Write>(&self, out: &mut Output<W, Mysql>) -> serialize::Result {
				out.write_u32::<LittleEndian>(0)?;
				out.write_wkb(self).map_err(GeometryError::from)?;
				Ok(IsNull::No)
			}
		}

		impl From<$type> for Geometry {
			fn from(other: $type) -> Self {
				Self {
					srid: other.srid,
					geom: geo_types::Geometry::$geom(other.geom),
				}
			}
		}

		impl TryFrom<Geometry> for $type {
			type Error = <geo_types::$geom<f64> as TryFrom<geo_types::Geometry<f64>>>::Error;

			fn try_from(other: Geometry) -> Result<Self, Self::Error> {
				Ok(Self {
					srid: other.srid,
					geom: other.geom.try_into()?,
				})
			}
		}
	};
}

mysql_type!(
	Point,
	Point,
	sql_types::Point,
	"A single point in 2D space."
);
mysql_type!(
	Polygon,
	Polygon,
	sql_types::Polygon,
	"A bounded two-dimensional area."
);
mysql_type!(
	LineString,
	LineString,
	sql_types::LineString,
	"A linearly interpolated path between locations."
);
mysql_type!(
	MultiPoint,
	MultiPoint,
	sql_types::MultiPoint,
	"A collection of [`Point`][geo_types::Point]s."
);
mysql_type!(
	MultiLineString,
	MultiLineString,
	sql_types::MultiLineString,
	"A collection of [`LineString`][geo_types::LineString]s."
);
mysql_type!(
	MultiPolygon,
	MultiPolygon,
	sql_types::MultiPolygon,
	"A collection of [`Polygon`][geo_types::Polygon]s."
);
mysql_type!(
	GeometryCollection,
	GeometryCollection,
	sql_types::GeometryCollection,
	"A collection of [`Geometry`][geo_types::Geometry] types."
);
mysql_type!(
	Geometry,
	Geometry,
	sql_types::Geometry,
	"An arbitrary geometry."
);
mysql_type!(
	BoundingBox,
	Rect,
	sql_types::Polygon,
	"An axis-aligned bounding rectangle.

MySQL doesn't support native `Rect`s. Corresponding functions like `ST_Envelope()` return
a `Polygon` of the shape `POLYGON((MINX MINY, MAXX MINY, MAXX MAXY, MINX MAXY, MINX MINY))`
instead. This struct exists to provide more convenient access to the coordinates if your
column always contains such polygons.

**Warning**: This can only be unserialized from a [`Polygon`][sql_types::Polygon]
column. If polygons in other shapes are unserialized, the process may either fail
or silently result in wrong bounds.
"
);

impl_traits!(Point, Point, sql_types::Point);
impl_traits!(Polygon, Polygon, sql_types::Polygon);
impl_traits!(LineString, LineString, sql_types::LineString);
impl_traits!(MultiPoint, MultiPoint, sql_types::MultiPoint);
impl_traits!(MultiLineString, MultiLineString, sql_types::MultiLineString);
impl_traits!(MultiPolygon, MultiPolygon, sql_types::MultiPolygon);
impl_traits!(
	GeometryCollection,
	GeometryCollection,
	sql_types::GeometryCollection
);

impl FromSql<sql_types::Geometry, Mysql> for Geometry {
	fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
		let mut bytes = not_none!(bytes);
		let srid = bytes.read_u32::<LittleEndian>()?;
		let mut bytes_cursor = Cursor::new(bytes);
		let geom = bytes_cursor.read_wkb().map_err(GeometryError::from)?;
		Ok(Self { srid, geom })
	}
}

impl ToSql<sql_types::Geometry, Mysql> for Geometry {
	fn to_sql<W: Write>(&self, out: &mut Output<W, Mysql>) -> serialize::Result {
		out.write_u32::<LittleEndian>(self.srid)?;
		out.write_wkb(&self.geom).map_err(GeometryError::from)?;
		Ok(IsNull::No)
	}
}

impl FromSql<sql_types::Polygon, Mysql> for BoundingBox {
	fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
		let polygon = <Polygon as FromSql<sql_types::Polygon, Mysql>>::from_sql(bytes)?;
		let mut coords = polygon.exterior().coords();
		let min = match coords.next() {
			None => return Err(Box::new(GeometryError::InvalidValue)),
			Some(c) => *c,
		};
		if coords.next().is_none() {
			return Err(Box::new(GeometryError::InvalidValue));
		}
		let max = match coords.next() {
			None => return Err(Box::new(GeometryError::InvalidValue)),
			Some(c) => *c,
		};
		Ok(Self {
			srid: polygon.srid,
			geom: geo_types::Rect::new(min, max),
		})
	}
}

impl ToSql<sql_types::Polygon, Mysql> for BoundingBox {
	fn to_sql<W: Write>(&self, out: &mut Output<W, Mysql>) -> serialize::Result {
		out.write_u32::<LittleEndian>(self.srid)?;
		out.write_wkb(&geo_types::Geometry::Polygon(self.to_polygon()))
			.map_err(GeometryError::from)?;
		Ok(IsNull::No)
	}
}

impl From<BoundingBox> for Geometry {
	fn from(other: BoundingBox) -> Self {
		Self {
			srid: other.srid,
			geom: geo_types::Geometry::Polygon(other.geom.to_polygon()),
		}
	}
}

impl From<BoundingBox> for Polygon {
	fn from(other: BoundingBox) -> Self {
		Self {
			srid: other.srid,
			geom: other.geom.to_polygon(),
		}
	}
}

impl Geometry {
	/// Construct an empty geometry
	///
	/// As MySQL only supports empty geometry collections this is basically equivalent to
	/// `Geometry::from(GeometryCollection::empty())`.
	#[inline]
	#[must_use]
	pub const fn empty(srid: SRID) -> Self {
		Self {
			srid,
			geom: geo_types::Geometry::GeometryCollection(geo_types::GeometryCollection(vec![])),
		}
	}
}

impl GeometryCollection {
	/// Construct an empty geometry collection
	#[inline]
	#[must_use]
	pub const fn empty(srid: SRID) -> Self {
		Self {
			srid,
			geom: geo_types::GeometryCollection(vec![]),
		}
	}
}

impl Default for GeometryCollection {
	fn default() -> Self {
		Self::empty(0)
	}
}

impl Default for Geometry {
	fn default() -> Self {
		Self::empty(0)
	}
}
